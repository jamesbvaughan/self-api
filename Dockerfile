FROM node:alpine
LABEL maintainer="James Vaughan <james@jamesbvaughan.com>"

WORKDIR /app

COPY package.json .
COPY package-lock.json .
RUN npm install

COPY . .

HEALTHCHECK CMD wget -q -O /dev/null http://localhost:3000 || exit 1

CMD ["npm", "start"]
