const fetch = require('node-fetch')
const qs = require('querystring')

const lastfmURL = 'https://ws.audioscrobbler.com/2.0/?' + qs.stringify({
  method: 'user.getrecenttracks',
  limit: 1,
  user: process.env.LAST_FM_USERNAME,
  format: 'json',
  api_key: process.env.LAST_FM_API_KEY,
})

const letterboxdURL = 'https://api.rss2json.com/v1/api.json?' + qs.stringify({
  rss_url: `https://letterboxd.com/${process.env.LETTERBOXD_USERNAME}/rss/`,
  api_key: process.env.RSS_2_JSON_API_KEY,
})

module.exports = (req, res) => {
  switch (req.url) {
    case '/song':
      fetch(lastfmURL)
        .then(r => r.json())
        .then(({ recenttracks: { track } }) => {
          const linkBody = `${track[0].name} by ${track[0].artist['#text']}`
          const link = `<a href='${track[0].url}'>${linkBody}</a>`
          const responseText = track.length > 1
            ? 'at the moment I\'m listening to '
            : 'the last song I listened to was '

          res.end(responseText + link)
        })
      break
    case '/movie':
      fetch(letterboxdURL)
        .then(r => r.json())
        .then(({ items: [movie] }) => {
          const link = `<a href="${movie.link}">${movie.title.match(/(.*),/)[1]}</a>`

          res.end('the last movie I watched was ' + link)
        })
      break
    default:
      res.end(`what are you doing at ${req.url}?`)
  }
}
